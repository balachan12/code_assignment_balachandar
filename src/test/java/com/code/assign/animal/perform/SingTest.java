package com.code.assign.animal.perform;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.serviceimpl.SingImpl;

public class SingTest {

	private static final String MESSAGE = "I can sing";
	@Test
	public void testWalk(){
		ISing walk = new SingImpl();
		String message = walk.sing();
		assertEquals(MESSAGE, message);
	}
}
