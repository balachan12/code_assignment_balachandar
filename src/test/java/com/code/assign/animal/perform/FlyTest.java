package com.code.assign.animal.perform;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.code.assign.animal.perform.service.IFly;
import com.code.assign.animal.perform.serviceimpl.FlyImpl;

public class FlyTest {

	private static final String MESSAGE = "I can fly";
	@Test
	public void testFly(){
		IFly fly = new FlyImpl();
		String message = fly.fly();
		assertEquals(MESSAGE, message);
	}
}
