package com.code.assign.animal.perform;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.code.assign.animal.perform.service.IWalk;
import com.code.assign.animal.perform.serviceimpl.WalkImpl;

public class WalkTest {

	private static final String MESSAGE = "I can walk";
	@Test
	public void testWalk(){
		IWalk walk = new WalkImpl();
		String message = walk.walk();
		assertEquals(MESSAGE, message);
	}
}
