package com.code.assign.animal;

import com.code.assign.animal.perform.service.IFish;
import com.code.assign.animal.perform.service.ISwim;
import com.code.assign.animal.perform.serviceimpl.SwimImpl;

public class Fish extends Animal {

	private ISwim swimPerform;
	

	String size = null;
	String color = null;

	public Fish() {
	    setAnimalType("Animal type: Fish");
		swimPerform = new SwimImpl();
	}

	public void setSwimPerform(ISwim swim) {
		swimPerform = swim;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void swim() {
		swimPerform.swim();
	}
}
