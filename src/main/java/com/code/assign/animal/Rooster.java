package com.code.assign.animal;

import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.serviceimpl.RoosterImpl;

public class Rooster {
	Chicken rooster = new Chicken();
	public Rooster(){		
		ISing singBehavior = new RoosterImpl();
		rooster.setSingPerform(singBehavior);
		rooster.setAnimalType("Animal Type Bird: Rooster");
		
	}
	
	public void fly(){
		rooster.fly();
	}
	
	public void walk(){
		rooster.walk();
	}
	
	public void sing(){
		rooster.sing();
	}
	
	
}
