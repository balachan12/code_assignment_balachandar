package com.code.assign.animal;

import com.code.assign.animal.perform.service.IFly;
import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.service.ISwim;
import com.code.assign.animal.perform.service.IWalk;
import com.code.assign.animal.perform.serviceimpl.FlyImpl;
import com.code.assign.animal.perform.serviceimpl.SingImpl;
import com.code.assign.animal.perform.serviceimpl.SwimImpl;
import com.code.assign.animal.perform.serviceimpl.WalkImpl;

public class Bird extends Animal{
	private IFly flyPerform;
	private IWalk walkPerform;
	private ISing singPerform;
	private ISwim swimPerform;
	
	public void fly(){
	    flyPerform.fly();
	}
	
	public void walk(){
	    walkPerform.walk();
	}
	
	public void sing(){
	    singPerform.sing();
	}
	
	public void swim(){
        swimPerform.swim();
    }
	public void setFlyPerform(IFly flyPerform) {
		this.flyPerform = flyPerform;
	}
	public void setWalkPerform(IWalk walkPerform) {
		this.walkPerform = walkPerform;
	}

	public void setSingPerform(ISing singPerform) {
		this.singPerform = singPerform;
	}
	public void setSwimPerform(ISwim singPerform) {
        this.swimPerform = swimPerform;
    }
	public Bird(){
		flyPerform = new FlyImpl();
		setFlyPerform(flyPerform);
		
		singPerform = new SingImpl();
		setSingPerform(singPerform);
		
		walkPerform = new WalkImpl();
		setWalkPerform(walkPerform);
		
		swimPerform = new SwimImpl();
        setSwimPerform(swimPerform);
		
		setAnimalType("Animal Type Bird");
	}
}
