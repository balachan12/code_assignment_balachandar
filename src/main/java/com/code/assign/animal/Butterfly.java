package com.code.assign.animal;

import com.code.assign.animal.perform.service.IFly;
import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.serviceimpl.ButterflyImpl;
import com.code.assign.animal.perform.serviceimpl.CluckImpl;
import com.code.assign.animal.perform.serviceimpl.NoFlyImpl;

public class Butterfly extends Bird {
    
public Butterfly(){
        
        IFly flyPerform = new ButterflyImpl();
        setFlyPerform(flyPerform); 
        
        ISing singPerform = new ButterflyImpl();
        setSingPerform(singPerform);
        
        setAnimalType("Animal type Bird:  Butterfly");
    }

}
