package com.code.assign.animal;


import com.code.assign.animal.perform.service.IFish;
import com.code.assign.animal.perform.serviceimpl.OtherFishImpl;

public class Shark extends Fish{

	IFish perform ;
	public Shark(){
		setColor("grey");
		setSize("large");
		perform = new OtherFishImpl();
		
		setAnimalType("Sharks are  " + size + " and " + color );
	}
	
	public void eat(){
	    perform.perform();
	}
	
}
