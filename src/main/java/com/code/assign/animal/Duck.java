package com.code.assign.animal;

import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.service.ISwim;
import com.code.assign.animal.perform.serviceimpl.QuackImpl;
import com.code.assign.animal.perform.serviceimpl.SwimImpl;

public class Duck extends Bird{

	public Duck(){
		ISing singPerform = new QuackImpl();
		ISwim swimPerform = new SwimImpl();
		
		setSingPerform(singPerform);
		setSwimPerform(swimPerform);
		
		setAnimalType("Animal type Bird: Duck");
	}
}
