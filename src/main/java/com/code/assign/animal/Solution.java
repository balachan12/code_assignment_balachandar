package com.code.assign.animal;

import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.serviceimpl.CatSoundImpl;
import com.code.assign.animal.perform.serviceimpl.DogSoundImpl;
import com.code.assign.animal.perform.serviceimpl.RoosterImpl;

public class Solution {

	public static void main(String[] args) {
	    System.out.println("Code Assignment Balachandar");

		System.out.println("*****************************************************");
		Bird bird = new Bird();
		bird.printAnimalType();
		bird.walk();		
		bird.sing();

		System.out.println();
		System.out.println("******************************************************");
		Bird duck = new Duck();
		duck.printAnimalType();
		duck.sing();
		duck.swim();
		

		System.out.println();
		System.out.println("******************************************************");
		Bird chicken = new Chicken();
		chicken.printAnimalType();
		chicken.sing();		
		chicken.fly();
		

		System.out.println();
		System.out.println("******************************************************");
		Rooster rooster = new Rooster();
		rooster.rooster.printAnimalType();
		rooster.walk();
		rooster.fly();
		rooster.sing();

		System.out.println();
		System.out.println("******************************************************");
		System.out.println("A Parrot living with dogs says");
		Bird parrotWithDog = new Parrot();
		ISing dogSound = new DogSoundImpl();
		parrotWithDog.setSingPerform(dogSound);		
		parrotWithDog.sing();
		
		

		System.out.println();
		System.out.println("******************************************************");
		System.out.println("A Parror living with cat says");
		Bird parrotWithCat = new Parrot();
		ISing catSound = new CatSoundImpl();
		parrotWithCat.setSingPerform(catSound);		
		parrotWithCat.sing();
		
		

		System.out.println();
		System.out.println("******************************************************");
		System.out.println("A parrot living near the rooster says");
		Bird parrotWithRooster = new Parrot();
		RoosterImpl roosterSound = new RoosterImpl();
		parrotWithRooster.setSingPerform(roosterSound);		
		parrotWithRooster.sing();
		
		

		System.out.println();
		System.out.println("******************************************************");

		Fish fish = new Fish();
		fish.printAnimalType();
		fish.swim();

		System.out.println();
		System.out.println("******************************************************");

		Shark shark = new Shark();
		shark.printAnimalType();
		shark.swim();
		shark.eat();

		System.out.println();
		System.out.println("******************************************************");

		Clownfish clownfish = new Clownfish();
		clownfish.printAnimalType();
		clownfish.swim();
		clownfish.makeJoke();

		System.out.println();
		System.out.println("******************************************************");
		Dolphin dolphin = new Dolphin();
		dolphin.printAnimalType();
		dolphin.swim();
		
		System.out.println();
        System.out.println("******************************************************");
        Bird butterfly = new Butterfly();
        butterfly.printAnimalType();
        butterfly.fly();
        butterfly.sing();     
       


	}

}
