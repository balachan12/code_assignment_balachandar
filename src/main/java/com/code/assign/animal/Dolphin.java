package com.code.assign.animal;

import com.code.assign.animal.perform.service.ISwim;
import com.code.assign.animal.perform.serviceimpl.SwimImpl;

public class Dolphin extends Animal{

	private ISwim swimPerform;
	public Dolphin(){
	    swimPerform = new SwimImpl();
		setAnimalType("Dolphins are not exactly fish, yet, they are good swimmers ");
	}
	
	public void swim(){
	    swimPerform.swim();
	}
}
