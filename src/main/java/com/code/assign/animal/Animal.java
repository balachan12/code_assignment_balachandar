package com.code.assign.animal;


public abstract class Animal {

	private String animalType;	

	public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public void printAnimalType(){
		System.out.println(animalType);
	}
}
