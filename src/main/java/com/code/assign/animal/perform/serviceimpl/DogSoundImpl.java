package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;

public class DogSoundImpl implements ISing{

	private String message;
	
	@Override
	public String sing() {
		System.out.println("Woof, woof");
		return message;
	}

}
