package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.IFly;

public class NoFlyImpl implements IFly {

	private String message = "can not fly";
	
	@Override
	public String fly() {
		System.out.println(message);
		return message;
	}

}
