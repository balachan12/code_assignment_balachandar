package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;

public class CluckImpl implements ISing {

	private String message = "A chicken says: Cluck, cluck";
	
	@Override
	public String sing() {
		System.out.println(message);
		return message;
	}

}
