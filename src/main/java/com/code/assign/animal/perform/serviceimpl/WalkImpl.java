package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.IWalk;

public class WalkImpl implements IWalk {

	private String message = "I can walk";
	
	@Override
	public String walk() {
		System.out.println(message);
		return message;
	}

}
