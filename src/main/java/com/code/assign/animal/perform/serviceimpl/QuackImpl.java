package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;

public class QuackImpl implements ISing{

	private String message = "A duck says : Quack, quack";
	
	@Override
	public String sing() {
		System.out.println(message);
		return message;
	}

}
