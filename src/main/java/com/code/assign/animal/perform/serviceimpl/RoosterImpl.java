package com.code.assign.animal.perform.serviceimpl;

import java.util.HashMap;
import java.util.Map;

import com.code.assign.animal.perform.service.ISing;

public class RoosterImpl implements ISing {

	private String message = "Cock-a-doodle-doo...";
	
	@Override
	public String sing() {
        System.out.println(message);
        return message;
    }
	
}
