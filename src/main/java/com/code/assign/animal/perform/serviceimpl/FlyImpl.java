package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.IFly;

public class FlyImpl implements IFly {

	private String message = "I can fly";
	
	@Override
	public String fly() {
		System.out.println(message);
		return message;
	}
	

}
