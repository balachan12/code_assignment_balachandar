package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.IFly;
import com.code.assign.animal.perform.service.ISing;

public class ButterflyImpl implements IFly,ISing{

    @Override
    public String fly() {
        System.out.println("Butterfly can fly");
        return "Butterfly can fly";
    }

    @Override
    public String sing() {
        System.out.println("Butterfly does not make a sound");
        return "Butterfly does not make a sound";
    }

}
