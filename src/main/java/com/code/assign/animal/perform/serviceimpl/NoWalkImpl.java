package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.IWalk;

public class NoWalkImpl implements IWalk {

    private String message = "Fishes don't walk";
    
    @Override
    public String walk() {
        System.out.println(message);
        return message;
    }
}
