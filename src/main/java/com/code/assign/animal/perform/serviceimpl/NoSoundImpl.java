package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;

public class NoSoundImpl implements ISing {

	private String message = "I do not make sounds...";
	
	@Override
	public String sing() {
		System.out.println(message);
		return message;
	}

}
