package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;

public class CatSoundImpl implements ISing {

	private String message = "Meow...";
	
	@Override
	public String sing() {
		System.out.println(message);
		return message;
	}

}
