package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.service.ISwim;

public class NoSingImpl implements ISing {

    private String message = "Fishes Don't sing";
    
    @Override
    public String sing() {
        System.out.println(message);
        return message;
    }
}
