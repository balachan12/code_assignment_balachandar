package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISwim;

public class SwimImpl implements ISwim {

	private String message = "I can swim";
	
	@Override
	public String swim() {
		System.out.println(message);
		return message;
	}

}
