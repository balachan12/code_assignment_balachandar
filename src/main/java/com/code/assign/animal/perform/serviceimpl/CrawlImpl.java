package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.IWalk;

public class CrawlImpl implements IWalk {

	private String message = "I am Crawling...";
	
	@Override
	public String walk() {
		System.out.println(message);
		return message;
	}

}
