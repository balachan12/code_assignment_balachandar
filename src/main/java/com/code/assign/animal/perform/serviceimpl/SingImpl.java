package com.code.assign.animal.perform.serviceimpl;

import com.code.assign.animal.perform.service.ISing;

public class SingImpl implements ISing {

	private String message = "I can sing";
	
	@Override
	public String sing() {
		System.out.println(message);
		return message;
	}

}
