package com.code.assign.animal;

import com.code.assign.animal.perform.service.IFish;
import com.code.assign.animal.perform.serviceimpl.MakeJokeImpl;

public class Clownfish extends Fish{

    IFish perform;
	public Clownfish(){
		setColor("orange");
		setSize("small");

		perform = new MakeJokeImpl();
		setAnimalType("Clownfish are  " + size + " and " + color);
	}
	
	public void makeJoke(){
	    perform.perform();
	}
}
