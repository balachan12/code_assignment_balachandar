package com.code.assign.animal;

import com.code.assign.animal.perform.service.IFly;
import com.code.assign.animal.perform.service.ISing;
import com.code.assign.animal.perform.serviceimpl.CluckImpl;
import com.code.assign.animal.perform.serviceimpl.NoFlyImpl;

public class Chicken extends Bird{

	public Chicken(){
		
		IFly flyPerform = new NoFlyImpl();
		setFlyPerform(flyPerform); 
		
		ISing singPerform = new CluckImpl();
		setSingPerform(singPerform);
		
		setAnimalType("Animal type Bird:  Chicken");
	}
}
